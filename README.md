## How to run this project

* clone this project with `git clone https://gitlab.com/fadilnatakusumah/movie-gallery.git`
* cd to `movie-gallery`
* run `npm install` or `yarn install`
* then run `npm run start` or `yarn start`


### About the project
Movie gallery that can look on movie details, save your favorite movies. Also built with autocomplete search feature with date filter.

### Desain UI inspiration 
[Desain #1](https://miro.medium.com/max/1600/1*ga8i7wWF-zMKvpPM1LXMMw.png)
[Desain #2](https://cdn.dribbble.com/users/1840454/screenshots/6542878/movie_detail.jpg)

### Final Results:
Screenshot 1
<img src="screenshots/sc1.PNG"> 
<br>
<br>

Screenshot 2
<img src="screenshots/sc2.PNG"> 
<br>
<br>

Screenshot 3
<img src="screenshots/sc3.PNG"> 
<br>
<br>

Screenshot 4
<br>
<img src="screenshots/sc4.PNG"> 
import React, { lazy, Suspense } from 'react'
import styled from '@emotion/styled';
import { observer } from 'mobx-react';

import { LIST_TYPE } from '../constants/list_type';
import store from '../store/movie';

const MovieCard = lazy(() => import('../components/MovieCard/MovieCard'));
const Searchbar = lazy(() => import('../components/Searchbar/Searchbar'));
const Modal = lazy(() => import('../components/Modal/Modal'));

class MoviesContainer extends React.Component {
  componentDidMount() {
    store.getMovies();
    store.getGenres();
  }

  renderTitle = () => {
    switch (store.listType) {
      case LIST_TYPE.FAVORITES:
        return <h1>Favorite Movies</h1>
      case LIST_TYPE.TOP_RATED:
        return <h1>Top Rated Movies</h1>
      case LIST_TYPE.SEARCHING:
        return <h1>Search Movies</h1>
      default:
        return <h1>Popular Movies</h1>
    }
  }

  render() {
    return (
      <MoviesContainerStyled>
        <Suspense fallback={null}>
          {store.selectedMovie && <Modal />}
          <div className="header-container">
            {this.renderTitle()}
            <div>
              <Searchbar />
            </div>
          </div>
          <div className="wrapper-grid">
            {store.movies.length === 0 && store.listType === LIST_TYPE.FAVORITES && (
              <h3>No saved movie</h3>
            )}
            {store.movies.map(data => (
              <MovieCard
                key={data.id}
                data={data}
              />
            ))}
          </div>
          <div className="showmore-container">
            {store.pagination.page < store.pagination.total_pages
              && !store.loadingMovies
              && store.listType !== LIST_TYPE.FAVORITES
              ? (
                <button className="button-style" onClick={store.showMore}>Show More</button>
              )
              : store.loadingMovies && <div className="button-style">Loading...</div>
            }
          </div>
        </Suspense>
      </MoviesContainerStyled>
    )
  }
}


const MoviesContainerStyled = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  
  @media (max-width: 600px){
    padding-left: 50px; 
    grid-template-columns: 1fr;
  }

  .showmore-container{
    text-align: center;
    padding: 35px;

    .button-style {
      display: inline-block;
      background: #364D5A;
      color: white;
      border: 1px solid white;
      padding: 10px 20px;
      font-size: 15px;
      border-radius: 5px;
      cursor: pointer;
    }
  }

  .header-container{
    color: white;
    display: flex;
    align-items:center;
    padding: 45px 25px;

    @media (max-width: 950px){
      flex-direction: column;
      >h1{
        padding: 10px 0;
      }
    }
  }

  .wrapper-grid{
    max-width: 1600px;
    display:grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-gap: 15px;
    width: 100%;
    padding: 25px;

    >h3{
      color: white;
    }

    @media (max-width: 1300px){
      grid-template-columns: 1fr 1fr 1fr;
    }
    
    @media (max-width: 950px){
      padding: 0;
      grid-template-columns: 1fr 1fr;
    }
    
    @media (max-width: 600px){
      grid-template-columns: 1fr;
    }
  }
`;
export default observer(MoviesContainer)

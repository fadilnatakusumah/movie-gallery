import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import "./App.scss";

const Sidebar = React.lazy(() => import('./components/Sidebar/Sidebar'));
const Wrapper = React.lazy(() => import('./components/Wrapper/Wrapper'));
const MoviesContainer = React.lazy(() => import('./containers/Movies'));

function App() {
  return (
    <React.Suspense fallback={null}>
      <Wrapper>
        <Sidebar />
        <MoviesContainer />
      </Wrapper>
    </React.Suspense>
  );
}

export default App;

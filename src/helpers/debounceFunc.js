export function debounceFunc(func, delay) {
  let timeOut;
  return function (...args) {
    if (timeOut) {
      clearTimeout(timeOut);
    }
    timeOut = setTimeout(() => {
      func(...args);
    }, delay);
  };
}
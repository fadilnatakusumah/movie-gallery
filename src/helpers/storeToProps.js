import React from 'react'
import { Observer } from "mobx-react";

export const storeToProps = (Component) => (store) => () => {
  return (
    <Observer>
      {() => <Component {...store} />}
    </Observer>
  )
}
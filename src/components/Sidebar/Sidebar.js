import React, { useState } from 'react'
import styled from '@emotion/styled'
import { observer } from 'mobx-react'

import store from '../../store/movie'
import { LIST_TYPE } from '../../constants/list_type'
import { FaHeart, FaSnowflake, FaSplotch, FaWater } from 'react-icons/fa'

const isActive = (listType) => store.listType === listType;

const changeListType = (listType) => {
  if (listType === LIST_TYPE.FAVORITES) {
    store.setState('movies', store.favorites);
    store.setState('listType', listType);
  } else {
    store.getMovies(listType);
  }
};

const Sidebar = observer(() => {
  return (
    <React.Fragment>
      <SidebarStyled>
        <h1>
          <a href="/">Movie Gallery</a>
        </h1>
        <div className="menus">
          <div
            className={isActive(LIST_TYPE.FAVORITES) ? "active" : ""}
            onClick={() => changeListType(LIST_TYPE.FAVORITES)}
          >
            Favorites
          </div>
          <div
            className={isActive(LIST_TYPE.POPULAR) ? "active" : ""}
            onClick={() => changeListType(LIST_TYPE.POPULAR)}
          >
            Popular
          </div>
          <div
            className={isActive(LIST_TYPE.TOP_RATED) ? "active" : ""}
            onClick={() => changeListType(LIST_TYPE.TOP_RATED)}
          >
            Top Rated
          </div>
        </div>
      </SidebarStyled>
      <MobileSidebar />
    </React.Fragment>
  )
})

const MobileSidebar = () => {
  const [show, toggleShow] = useState(false);

  return (
    <ResponsiveSidebarStyled className={`${!show ? "hide-view" : ""}`}>
      <h4>
        <a href="/">Movie Gallery</a>
        <div className="hamburger-icons" onClick={() => toggleShow(!show)}>
          <FaWater />
        </div>
      </h4>
      <div className="menus">

        <div
          className={isActive(LIST_TYPE.FAVORITES) ? "active" : ""}
          onClick={() => {
            toggleShow(false);
            changeListType(LIST_TYPE.FAVORITES)
          }}
          title="Favorites Movies"
        >
          {show ? "Favorites" : <FaHeart />}
        </div>
        <div
          className={isActive(LIST_TYPE.POPULAR) ? "active" : ""}
          onClick={() => {
            toggleShow(false);
            changeListType(LIST_TYPE.POPULAR)
          }}
          title="Popular Movies"
        >
          {show ? "Popular" : <FaSplotch />}
        </div>
        <div
          className={isActive(LIST_TYPE.TOP_RATED) ? "active" : ""}
          title="Top Rated Movies"
          onClick={() => {
            toggleShow(false);
            changeListType(LIST_TYPE.TOP_RATED);
          }}
        >
          {show ? "Top Rated" : <FaSnowflake />}

        </div>
      </div>
    </ResponsiveSidebarStyled >
  )
}
const ResponsiveSidebarStyled = styled.div`
  &.hide-view{
    width: 50px;
    
    h4{
      display: flex;
      a{display: none;}

      >div{
        display: flex;
        align-items:center;
        font-size: 20px;
        cursor: pointer;
      }
    }

    .menus{
      .active{
        background-color: #283C46;
        border-left: 2px solid #82cc9b;
      }
    }
    
  }
  @media (min-width: 601px){
    display: none;
  }

  position:fixed;
  z-index: 1;
  bottom: 0;
  top:0;
  background-color: #1A2731;
  width: 250px;
  color: white;

  >h4{
    font-size: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding:10px;
    margin:0;
    a {
      color: #ccc;
      text-decoration: none;
    }
    >div{
      cursor: pointer;
    }
  }

  .menus{
    
    >div{
      padding: 10px;
      font-size: 18px;
      cursor: pointer;

      :hover{
        background-color: #2D414C;
      }
    }

    .active{
      background-color: #283C46;
      border-left: 2px solid #82cc9b;
    }
  }
`
const SidebarStyled = styled.div`
  @media (max-width: 601px){
    display: none;
  }

  background-color: #1A2731;
  width: 250px;
  color: white;

  >h1{
    padding:10px;
    margin:0;
    a {
      color: #ccc;
      text-decoration: none;
    }
  }

  .menus{
    
    >div{
      padding: 10px;
      font-size: 18px;
      cursor: pointer;

      :hover{
        background-color: #2D414C;
      }
    }

    .active{
      background-color: #283C46;
      border-left: 2px solid #82cc9b;
    }
  }
`

export default Sidebar

import * as React from 'react'
import Downshift from 'downshift'
import styled from '@emotion/styled';

const AutoComplete = ({ onSelectValue, onKeyDown, onChange, options = [], ...props }) => {
  const [optionValues, setOptionValues] = React.useState(options);

  React.useEffect(() => {
    setOptionValues(options)
  }, [options])

  const onChangeValue = (typedText) => onChange(typedText);

  const onSelectOption = (selection) => onSelectValue(selection)

  return (
    <Downshift
      onChange={onSelectOption}
      onInputValueChange={onChangeValue}
      itemToString={item => (item ? item.label : '')}
    >
      {({
        getInputProps,
        getItemProps,
        getLabelProps,
        getMenuProps,
        isOpen,
        inputValue,
        highlightedIndex,
        selectedItem,
        getRootProps,
        closeMenu,
      }) => (
          <AutoCompleteStyled {...getRootProps({}, { suppressRefError: true })}>
            <InputField {...getInputProps()} {...props} onKeyDown={(evt) => {
              onKeyDown(evt);
              if (evt.key === "Enter") {
                closeMenu();
                // setTimeout(() => {
                // }, 1000);
              }
            }} />
            <ListContainer {...getMenuProps()} >
              {isOpen
                ? optionValues
                  .filter(item => !inputValue || item.label.toLowerCase().includes(inputValue.toLowerCase()))
                  .slice(0, 10)
                  .map((item, index) => (
                    <ListOption
                      {...getItemProps({
                        key: item.id,
                        index,
                        item,
                        // style: {
                        //   backgroundColor:
                        //     highlightedIndex === index ? 'lightgray' : 'white',
                        //   fontWeight: selectedItem === item ? 'bold' : 'normal',
                        // },
                      })}
                    >
                      {item.label}
                    </ListOption>
                  ))
                : null}
            </ListContainer>
          </AutoCompleteStyled>
        )}
    </Downshift>
  )
}

const AutoCompleteStyled = styled.div`
  position: relative;
`

const ListContainer = styled.ul`
  border-radius: 10px;
  margin: 0;
  margin-top: 3px;
  position: absolute;
  left: 0;
  right: 0;
  list-style: none;
`

const ListOption = styled.li`
  margin: 0;
  font-size: 18px;
  padding: 10px;
  cursor: pointer;

  background-color: white;
  :hover{
    background-color: #f5f5f5;
  }
`

const InputField = styled.input`
  height: 35px;
  padding: 0 10px;
  border-radius: 5px;
  border: none;
  outline: none;
  font-size: 18px;
`

export default AutoComplete;
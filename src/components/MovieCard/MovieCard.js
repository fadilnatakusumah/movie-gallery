import React, { memo } from 'react'
import styled from '@emotion/styled';
import { observer } from 'mobx-react';

import ImageViewer from '../ImageViewer/ImageViewer';
import store from '../../store/movie';

const MovieCard = observer(({ data }) => {
  const getGenres = (ids = []) => {
    const findGenres = [];
    if (ids[0] && typeof ids[0] === "object") {
      store.genres.forEach((value) => ids.includes(value.id) ? findGenres.push(value) : null);

    }
    store.genres.forEach((value) => ids.includes(value.id) ? findGenres.push(value) : null);
    return findGenres;
  }

  const selectMovie = () => {
    store.setState('selectedMovie', data);
  }

  return (
    <MovieCardStyled>
      <ImageViewer
        className={"poster-style"}
        src={data.poster_path}
        onClick={selectMovie}
      />
      <div className={"movie-description"}>
        <div onClick={selectMovie}>{data.title}</div>

        <div>{data.genres
          ? data.genres.slice(0, 2).map(({ name }) => name).join(',')
          : getGenres(data.genre_ids).slice(0, 2).map(({ name }) => name).join(', ')}</div>
      </div>
    </MovieCardStyled>
  )
})

const MovieCardStyled = styled.div`
  .poster-style{
    max-width: 250px;
    width: 100%;
    border-radius: 15px;
    box-shadow: 2px 2px 13px -5px;
    cursor: pointer;
  }
    
  @media (max-width: 950px){
    display: flex;
    flex-direction: column;
    align-items: center;
    .movie-description{
      text-align:center !important;
    }
  }


  .movie-description{
    text-align: left;
    >div:first-of-type{ 
      max-width:250px;
      color: white;
      font-size: 18px;
      font-weight: 600;
      cursor: pointer;
    }
    >div:last-child{ 
      color: gray;
      font-size: 14px;
    }
  }
`;

export default memo(MovieCard);

import dayjs from 'dayjs';
import { observer } from 'mobx-react';
import React from 'react'
import { Modal } from 'react-bootstrap';
import { FaBookmark, FaTimesCircle } from 'react-icons/fa';

import store from '../../store/movie';
import ImageViewer from '../ImageViewer/ImageViewer';
import './Modal.scss';

class ModalComponent extends React.Component {
  state = {
    detail: null,
    loading: false
  }
  componentDidMount() {
    this.setState({ loading: true })
    store.getDetailMovie(store.selectedMovie.id)
      .then(({ data }) => {
        this.setState({ detail: data, loading: false })
      }).catch(err => {
        console.error("ModalComponent -> componentDidMount -> err", err)
        this.setState({ detail: null, loading: false })
      });
  }

  closeModal = () => {
    store.setState("selectedMovie", null);
    this.setState({ detail: null })
  }

  saveToFavorites = (movie) => store.saveToFavorites(movie)

  renderContent = () => {
    const { detail } = this.state
    if (!detail) {
      return null;
    }

    return (
      <div>
        <div className="modal__header">
          <ImageViewer src={detail.poster_path} />
          {/* <img src="https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg" /> */}
          <div className="close-button" onClick={this.closeModal}>
            <FaTimesCircle />
          </div>
          <div className={`save-button ${store.isAlreadSaved(detail.id) ? "save-button__active" : ""}`} onClick={() => this.saveToFavorites(detail)}>
            <FaBookmark />
          </div>
        </div>
        <div className="modal__separator"></div>
        <div className="modal__content">
          <div className="title">
            <h1>{detail.title}</h1>
            <h4>{detail.tagline}</h4>
          </div>
          <div className="attributes">
            <div>
              <div>{detail.runtime} min</div>
              <div>Duration</div>
            </div>
            <div>
              <div>{dayjs(detail.release_date).format("DD-MMM-YYYY")}</div>
              <div>Release</div>
            </div>
            <div>
              <div>{detail.status}</div>
              <div>Status</div>
            </div>
            <div>
              <div>{detail.vote_average}</div>
              <div>Rating</div>
            </div>
          </div>
          <div className="overview">
            <h4>Overview</h4>
            <p>{detail.overview}</p>
          </div>
          <div className="genres">
            <h5>Genres</h5>
            <div>
              {detail.genres.map((genre, key) => <div key={key}>{genre.name}</div>)}
            </div>
          </div>
          {detail.homepage &&
            <div className="links">
              <a href={detail.homepage} rel="noopener noreferrer" target="_blank">Go to Movie's HomePage</a>
            </div>}
        </div>
      </div>
    )
  }

  render() {
    return (
      <Modal dialogClassName="modal-90w" show={true} onHide={this.closeModal}>
        {this.state.loading
          ? <div className="loading-view">
            <h2>Loading...</h2>
          </div>
          : this.renderContent()
        }
      </Modal>
    )
  }
}

export default observer(ModalComponent)

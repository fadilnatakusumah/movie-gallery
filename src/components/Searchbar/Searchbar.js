import styled from '@emotion/styled';
import React from 'react'
import { FaSearch, FaTimes } from 'react-icons/fa';
import { observer } from 'mobx-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import AutoComplete from '../AutoComplete/AutoComplete';
import store from '../../store/movie';
import { formatDate, parseDate } from '../../Utils/dateUtils';
import { searchMovies } from '../../api/movies';
import { LIST_TYPE } from '../../constants/list_type';
import { debounceFunc } from '../../helpers/debounceFunc';

class Searchbar extends React.Component {
  state = {
    searchText: "",
    date: "",
    options: []
  }

  onSubmitSearch = () => {
    const { searchText, date } = this.state;
    let params = {}
    if (searchText === "" && date !== "") {
      params = {
        query: new Date(date).getFullYear(),
      }
    } else if (searchText !== "" && date !== "") {
      params = {
        query: searchText,
        year: new Date(date).getFullYear(),
      }
    } else if (searchText !== "" && date === "") {
      params = {
        query: searchText,
      }
    }
    if (Object.keys(params).length > 0) {
      store.searchMoviesWithQuery(params);
    } else {
      store.getMovies();
    }
  }

  onChangeValue = (text) => {
    this.setState({ searchText: text })
    this.getMovies();
  }

  getMovies = debounceFunc(() => {
    if (this.state.searchText !== "") {
      searchMovies({ query: this.state.searchText })
        .then(res => {
          const { results = [] } = res.data.data;
          this.setState({ options: results.map(value => ({ ...value, label: value.title })) })
        })
    }
  }, 600)

  onSelectValue = (selection) => {
    store.setState('selectedMovie', selection)
    store.setState('listType', LIST_TYPE.SEARCHING);
    store.setState('movies', this.state.options);
  }

  onEnterKey = () => {
    if (this.state.options.length > 0) {
      this.setState({ searchText: '' })
      store.setState('movies', this.state.options);
    } else {
      this.onSubmitSearch()
      this.setState({ searchText: '' })
    }
  }

  render() {
    const { options, date, searchText } = this.state;
    return (
      <SearchbarStyled>
        <div className="input-container">
          <DayPickerInput
            format={"dd-MM-yyyy"}
            formatDate={formatDate}
            parseDate={parseDate}
            placeholder="Select Date"
            value={date}
            onChange={(evt) => evt.preventDefault()}
            onDayChange={(val) => this.setState({ date: val })} />
          {date &&
            <button onClick={() => this.setState({ date: "" })}>
              <FaTimes />
            </button>}
        </div>
        <div className="input-container">
          <AutoComplete
            value={searchText}
            placeholder="Search movie"
            onChange={this.onChangeValue}
            onKeyDown={({ key }) => key === "Enter" ? this.onEnterKey() : null}
            onSelectValue={this.onSelectValue}
            options={options}
          />
          {/* <input
            placeholder="Search movie"
            type="text"
            value={searchText}
            onKeyDown={({ key }) => key === "Enter" ? this.onSubmitSearch() : null}
            onChange={({ target }) => this.setState({ searchText: target.value })}
          /> */}
          <button onClick={this.onSubmitSearch}>
            <FaSearch />
          </button>
        </div>
      </SearchbarStyled>
    )
  }
}

const SearchbarStyled = styled.div`
  display: flex;
  margin: 0 20px;
  color: black;
    
  @media (max-width: 950px){
    flex-direction: column;
    gap: 10px;
  }

  .input-container{
    margin: 0 10px;
    background-color: white;
    border-radius: 5px;
    border: none;
    outline: none;
    font-size: 18px;
    display: flex;
    align-items: center;
    height: 35px;

    .DayPickerInput{
      height: 100%;
      >input {
        padding: 0 10px;
        border-radius: 5px;
        border: none;
        font-size: 18px;
        height: 100%;
      }
    }
  
    /* >input{
      height: 100%;
      padding: 0 10px;
      border-radius: 5px;
      border: none;
      outline: none;
      font-size: 18px;
    } */

    >button{
      border: none;
      background: #ccc;
      height: 100%;
      padding: 0 10px;
      border-radius: 0 5px 5px 0;
      display: flex;
      align-items: center;
      cursor: pointer;
    }
  }


  button.search-button {
    border: none;
    background-color: #179074;
    color: white;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    box-shadow: 2px 2px 10px -5px black;
    margin: 0 15px;
    transition: .5s;
    cursor: pointer;
    &:hover{
      background-color:#1fbf9a;
      box-shadow: 2px 2px 10px -5px white;
    }
  }
  
  button.clear-button {
    border: none;
    background-color: maroon;
    color: white;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    box-shadow: 2px 2px 10px -5px black;
    margin: 0 15px;
    transition: .5s;
    cursor: pointer;
    &:hover{
      background-color:red;
      box-shadow: 2px 2px 10px -5px white;
    }
  }
`

export default observer(Searchbar)

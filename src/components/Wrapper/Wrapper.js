import React from 'react'

function Wrapper({ children, ...props }) {
  return (
    <div className="App" {...props}>
      {children}
    </div>
  )
}


export default Wrapper

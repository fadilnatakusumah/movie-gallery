import React from 'react'

const BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w500";

function ImageViewer({ src, ...props }) {
  return (
    <img alt={src} src={`${BASE_IMAGE_PATH}${src}`} {...props} />
  )
}

export default ImageViewer

import axios from 'axios';
import { LIST_TYPE } from '../constants/list_type';

const API_KEY = "63515dab9de95fef3c3b92697df50153";
const BASE_URL = "https://api.themoviedb.org";

export const getMoviesList = (type = LIST_TYPE.POPULAR, params = {}) => {
  return axios.get(`${BASE_URL}/3/movie/${type}`, {
    params: {
      api_key: API_KEY,
      ...params
    }
  })
    .then(res => ({
      success: true,
      data: res
    }))
    .catch(err => ({
      success: false,
      error: err,
    }));
}

export const getDetailMovie = id => {
  return axios.get(`${BASE_URL}/3/movie/${id}`, {
    params: {
      api_key: API_KEY,
    }
  })
    .then(res => ({
      success: true,
      data: res
    }))
    .catch(err => ({
      success: false,
      error: err,
    }));
}

export const searchMovies = (params = {}) => {
  return axios.get(`${BASE_URL}/3/search/movie`, {
    params: {
      api_key: API_KEY,
      ...params
    }
  })
    .then(res => ({
      success: true,
      data: res
    }))
    .catch(err => ({
      success: false,
      error: err,
    }));
}

export const getGenresMovies = () => {
  return axios.get(`${BASE_URL}/3/genre/tv/list`, {
    params: {
      api_key: API_KEY,
    }
  })
    .then(res => ({
      success: true,
      data: res
    }))
    .catch(err => ({
      success: false,
      error: err,
    }));
}

export const getGenresSeries = () => {
  return axios.get(`${BASE_URL}/3/genre/movie/list`, {
    params: {
      api_key: API_KEY,
    }
  })
    .then(res => ({
      success: true,
      data: res
    }))
    .catch(err => ({
      success: false,
      error: err,
    }));
}
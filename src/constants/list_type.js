export const LIST_TYPE = {
  FAVORITES: "favorites",
  POPULAR: "popular",
  UPCOMING: "upcoming",
  TOP_RATED: "top_rated",
  SEARCHING: "search",
};
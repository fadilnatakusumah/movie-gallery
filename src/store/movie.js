// import React, { createContext, useState } from 'react';

import { getDetailMovie, getGenresMovies, getGenresSeries, getMoviesList, searchMovies } from "../api/movies";
import { LIST_TYPE } from "../constants/list_type";

const { action, observable, decorate } = require("mobx");

// import { useDataMovies } from '../hooks/movieHooks';

// const { Provider } = createContext({ movies: [], detail: null });

// export const AppProvider = ({ children }) => {
//   const data = useDataMovies();

//   return (
//     <Provider value={data}>
//       {children}
//     </Provider>
//   )
// }

class MovieStore {
  selectedMovie = null;
  loadingMovies = false;
  movies = [];
  genres = [];
  favorites = [];
  pagination = { page: 1, total_pages: 1, total_results: 0 };
  params = {};
  listType = LIST_TYPE.POPULAR;

  setState = (key, value) => this[key] = value;

  getDetailMovie = async (id) => {
    return new Promise((res, rej) => {
      if (!id) return rej(null);
      getDetailMovie(id)
        .then(({ data }) => res(data))
        .catch(err => rej(err))
    })
  }

  getMovies = async (type) => {
    this.setState('listType', type || LIST_TYPE.POPULAR)
    this.setState('loadingMovies', true);
    try {
      const response = await getMoviesList(this.listType);
      const { results, ...pagination } = response.data.data;
      this.setState('movies', results);
      this.setState('pagination', pagination);
      this.setState('loadingMovies', false);
    } catch (error) {
      this.setState('loadingMovies', false);
      console.error("ERROR", error);
    }
  }

  getGenres = async () => {
    try {
      const genresSeries = await getGenresSeries();
      const genresMovies = await getGenresMovies();
      const { genres: series } = genresSeries.data.data;
      const { genres: movies } = genresMovies.data.data;
      this.setState("genres", [...series, ...movies,])
    } catch (error) {

    }
  }

  searchMoviesWithQuery = async (params = {}) => {
    this.setState('loadingMovies', true);
    this.setState('listType', LIST_TYPE.SEARCHING);
    try {
      const response = await searchMovies({ ...params, page: 1 });
      const { results, ...pagination } = response.data.data;
      this.setState('movies', results);
      this.setState('pagination', pagination);
      this.setState('params', params);
      this.setState('loadingMovies', false);
    } catch (error) {
      this.setState('loadingMovies', false);
      console.error("ERROR", error);
    }
  }

  showMore = async () => {
    this.setState('loadingMovies', true);
    if (this.pagination.page < this.pagination.total_pages) {
      try {
        if (this.listType !== LIST_TYPE.SEARCHING) {
          const response = await getMoviesList(this.listType, { page: this.pagination.page + 1 });
          const { results } = response.data.data;
          this.setState('movies', [...this.movies, ...results])
          this.setState('loadingMovies', false);
        } else {
          const response = await searchMovies({ ...this.params, page: this.pagination.page + 1 });
          const { results, ...pagination } = response.data.data;
          this.setState('pagination', pagination);
          this.setState('movies', [...this.movies, ...results])
          this.setState('loadingMovies', false);
        }
      } catch (error) {
        console.error("error", error);
        this.setState('loadingMovies', false);
      }
    }
  }

  isAlreadSaved = (id) => {
    return this.favorites.find(movie => movie.id === id);
  }

  saveToFavorites = (movie) => {
    let newFavorites = [...this.favorites];
    if (this.isAlreadSaved(movie.id)) {
      newFavorites = newFavorites.filter(savedMovie => savedMovie.id !== movie.id);
      this.setState('movies', newFavorites);
    } else {
      newFavorites.push(movie);
    }
    this.setState('favorites', newFavorites);
  }
}

decorate(MovieStore, {
  selectedMovie: observable,
  loadingMovies: observable,
  movies: observable,
  genres: observable,
  favorites: observable,
  pagination: observable,
  listType: observable,
  setState: action,
  getMovies: action,
  getGenres: action,
  searchMoviesWithQuery: action,
  showMore: action,
})

const store = new MovieStore();
export default store;